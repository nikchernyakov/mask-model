// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "UObject/NoExportTypes.h"
#include "SocialMask.generated.h"

/**
 * 
 */
UCLASS(Blueprintable, DefaultToInstanced, EditInlineNew)
class MASKMODEL_API USocialMask : public UObject
{
	GENERATED_BODY()

    UPROPERTY(EditAnywhere)
    float Influence;
};
