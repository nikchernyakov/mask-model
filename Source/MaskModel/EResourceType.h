// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "EResourceType.generated.h"

/**
 * 
 */
UENUM(Blueprintable)
enum EResourceType
{
 Food        UMETA(DisplayName = "Food"),
 Water       UMETA(DisplayName = "Water"),
 House       UMETA(DisplayName = "House"),
 Money       UMETA(DisplayName = "Money"),
};