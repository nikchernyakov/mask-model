// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "UObject/NoExportTypes.h"
#include "BaseSocialGoal.generated.h"

/**
 * 
 */
UCLASS(Blueprintable, DefaultToInstanced, EditInlineNew)
class MASKMODEL_API UBaseSocialGoal : public UObject
{
	GENERATED_BODY()

public:

	UPROPERTY(BlueprintReadOnly, EditAnywhere)
    FName Name;
	
	UPROPERTY(BlueprintReadOnly, EditAnywhere)
	bool IsPeriodic;

	UPROPERTY(BlueprintReadOnly, EditAnywhere)
	float DeadlineTime = 0;

	UPROPERTY(BlueprintReadOnly, EditAnywhere, meta=(ClampMin="0", ClampMax="1"))
    float Importance;

	UPROPERTY(BlueprintReadWrite, EditAnywhere)
    float CurrentDeadlineTime;

	UPROPERTY(BlueprintReadWrite, EditAnywhere)
    float CurrentPriority = 0;
};
