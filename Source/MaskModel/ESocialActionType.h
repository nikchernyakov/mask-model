// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "ESocialActionType.generated.h"

/**
* 
*/
UENUM(Blueprintable)
enum ESocialActionType
{
	Mining        UMETA(DisplayName = "Mining"),
    Trade         UMETA(DisplayName = "Trade"),
};