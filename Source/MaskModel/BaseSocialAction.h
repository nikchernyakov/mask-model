// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "ESocialActionType.h"
#include "SocialResource.h"
#include "UObject/NoExportTypes.h"
#include "BaseSocialAction.generated.h"

/**
 * 
 */
UCLASS(Blueprintable, DefaultToInstanced, EditInlineNew)
class MASKMODEL_API UBaseSocialAction : public UObject
{
	GENERATED_BODY()

public:

	UPROPERTY(BlueprintReadOnly, EditAnywhere)
	TEnumAsByte<ESocialActionType> Type;

	UPROPERTY(BlueprintReadOnly, EditAnywhere)
    float ExecutionTime;
	
	UPROPERTY(BlueprintReadOnly, EditAnywhere)
    TMap<TEnumAsByte<EResourceType>, int> Requirements;

	UPROPERTY(BlueprintReadOnly, EditAnywhere)
    TMap<TEnumAsByte<EResourceType>, int> Incomes;

	/*UFUNCTION(BlueprintCallable, BlueprintImplementableEvent)
	void Start();

	UFUNCTION(BlueprintCallable, BlueprintImplementableEvent)
    void End();*/
};
