// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "BaseSocialAction.h"
#include "GameFramework/Character.h"
#include "SocialMask.h"
#include "BaseSocialCharacter.generated.h"

UCLASS()
class MASKMODEL_API ABaseSocialCharacter : public ACharacter
{
	GENERATED_BODY()

public:
	// Sets default values for this character's properties
	ABaseSocialCharacter();

	UPROPERTY(EditAnywhere, Instanced)
	class USocialMask* SocialMask;

	UPROPERTY(BlueprintReadOnly, EditAnywhere, Instanced)
	TArray<UBaseSocialAction*> SocialActions;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

};
