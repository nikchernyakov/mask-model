// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "EResourceType.h"
#include "UObject/NoExportTypes.h"
#include "SocialResource.generated.h"

/**
 * 
 */
UCLASS(Blueprintable, DefaultToInstanced, EditInlineNew)
class MASKMODEL_API USocialResource : public UObject
{
	GENERATED_BODY()

public:
	
	UPROPERTY(BlueprintReadOnly, EditAnywhere)
	TEnumAsByte<EResourceType> Type;

	UPROPERTY(BlueprintReadOnly, EditAnywhere)
    uint8 Count;
	
};
